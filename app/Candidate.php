<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $guarded = [];
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }
    public function path()
    {
        return route('candidate.show', $this);
    }
}
