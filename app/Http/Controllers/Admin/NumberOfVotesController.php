<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Candidate;
use App\NumberOfVotes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class NumberOfVotesController extends Controller
{
    public function edit()
    {
        $numberOfVotes = NumberOfVotes::find(1);
        if (!$numberOfVotes) {
            $numberOfVotes = new NumberOfVotes();
            $numberOfVotes->number_of_votes = 5;

            try {
                $numberOfVotes->save();
            } catch (Throwable $exception) {
                return redirect()->back()->withErrors(['update' => 'Błąd inicjalizacji danych. Spróbuj jeszcze raz!']);
            }
        }


        return view('admin.numberOfVotes', ['numberOfVotes' => $numberOfVotes]);
    }
    public function update(NumberOfVotes $numberOfVotes)
    {

        try {
            $numberOfVotes->update($this->validateNoV());
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors(['update' => 'Błąd zapisu kandydata. Spróbuj jeszcze raz!']);
        }

        return redirect(route('candidate.results'));
    }

    public function validateNoV()
    {
        return request()->validate([
            'number_of_votes' => 'required | integer'
        ]);
    }
}
