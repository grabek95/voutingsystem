<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Candidate;
use App\Http\Requests\CandidateRequest;
use App\Vote;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Throwable;

use function GuzzleHttp\Promise\all;

class CandidateController extends Controller
{
    public function index()
    {
        return view('admin.candidates.index', ['candidates' => Candidate::all()]);
    }

    public function show(Candidate $candidate)
    {

        return view('admin.candidates.show', ['candidate' => $candidate]);
    }

    public function create()
    {
        return view('admin.candidates.create');
    }
    public function store(CandidateRequest $request)
    {
        $candidate = new Candidate(request(['name','description']));
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image');
            $imageName = $imagePath->getClientOriginalName();
            $path = $request->file('image')->store('images', 'public');
            $candidate->image = $path;
        } else {
            $candidate->image = null;
        }
        $candidate->save();
        try {
            $candidate->save();
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors(['store' => 'Błąd zapisu kandydata. Spróbuj jeszcze raz!']);
        }
        return redirect(route('candidate.results'));
    }
    public function edit(Candidate $candidate)
    {

        return view('admin.candidates.edit', ['candidate' => $candidate]);
    }
    public function update(Candidate $candidate, CandidateRequest $request)
    {
        Storage::disk('public')->delete($candidate->image);
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image');
            $imageName = $imagePath->getClientOriginalName();
            $path = $request->file('image')->store('images', 'public');
        } else {
            $path = null;
        }

        $date = [
            'name' => $request->name,
            'description' => $request->description,
            'image' => $path
        ];

        try {
            $candidate->update($date);
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors(['update' => 'Błąd aktualizacji pozycji. Spróbuj jeszcze raz!']);
        }
        return redirect(route('candidate.results'));
    }
    public function destroy(Candidate $candidate)
    {

        try {
            Storage::disk('public')->delete($candidate->image);
            $candidate->delete();
        } catch (Throwable $exception) {
            return redirect()->back()->withErrors(['delete' => 'Błąd usuwania kandydata. Spróbuj jeszcze raz!']);
        }
        return redirect(route('candidate.results'));
    }
}
