<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\NumberOfVotes;
use App\Vote;
use App\Candidate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Request;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{

    public function destroy(Vote $vote)
    {
        //delete res

        try {
            $vote->delete();
        } catch (ModelNotFoundException $exception) {
            return redirect()->back()->withErrors(['delete' => 'Błąd usuwania głosu. Spróbuj jeszcze raz!']);
        }
        return redirect(route('candidate.results'));
    }
}
