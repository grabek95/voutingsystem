<?php

namespace App\Http\Controllers;

use App\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function index()
    {
        ///Rener a list
        return view('results', ['candidates' => Candidate::all()]);
    }
    public function results()
    {
        $candidates = Candidate::all();
        $data = [];
        foreach ($candidates as $candidate) {
            array_push($data, ['name' => $candidate->name,'votes' => count($candidate->votes)]);
        }

        return $data;
    }
}
