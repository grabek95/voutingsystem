<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoteRequest;
use App\NumberOfVotes;
use App\Vote;
use App\Candidate;
use http\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

class VoteController extends Controller
{


    public function show(Vote $id)
    {
    }
    public function create(Request $request)
    {
        //shows a view to create a new resource
        return view('voting', [
            'candidates' => Candidate::all()
        ]);
    }
    public function store(VoteRequest $request)
    {
        $ip = $this->getUserIpAddr();
        $query1 = Vote::where('ip_adress', $ip)->get();
        $numberOfVotes = NumberOfVotes::find(1);
        if (!$numberOfVotes) {
            $numberOfVotes = new NumberOfVotes();
            $numberOfVotes->number_of_votes = 5;
            $numberOfVotes->save();
        }
        if (count($query1) < $numberOfVotes->number_of_votes) {
            $votes = request('candidates');
            //dd($query2);
            if (Vote::where('ip_adress', $ip)->whereIn('candidate_id', $votes)->doesntExist()) {
                try {
                    DB::beginTransaction();
                    foreach ($votes as $vote) {
                        $toSave = new Vote();
                        $toSave->candidate_id = $vote;
                        $toSave->ip_adress = $ip;
                        $toSave->save();
                    }
                    DB::commit();
                } catch (Throwable $e) {
                    DB::rollback();
                    return redirect()->back()->withErrors([
                        'transaction' => 'Błąd zapisu do bazy.Spóbuj ponownie']);
                }
                return redirect(route('vote.results'));
            } else {
                return redirect()->back()->withErrors([
                    'duplication' => 'Był już oddany głos na jednego z kandydatów']);
            }
        } else {
            return redirect()->back()->withErrors(['morethan5' => 'Oddałeś już maksymalną liczbę głosów!']);
        }
    }

    public function getUserIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
