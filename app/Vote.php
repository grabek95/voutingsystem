<?php

namespace App;

use App\Candidate;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $guarded = [];

    public function candidate()
    {
        return $this->belongsTo(Candidate::class)->withTimestamps();
        ;
    }
}
