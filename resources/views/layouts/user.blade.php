@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">Menu</div>
                    <ul class="list-group list-group-flush ">
                        <form method="GET" action="http://www.example.com">
                            <li class="list-group-item "><a class="text-body" href={{route('user.home')}}>Home</a></li>
                            <li class="list-group-item "><a class="text-body" href={{route('user.home')}}>Dodaj kandydature</a></li>
                            <li class="list-group-item "><a class="text-body" href={{route('user.home')}}>Moja kandydatura</a></li>
                            <li class="list-group-item "><a class="text-body" href={{route('user.home')}}>Edytuj kandydature</a></li>
                        </form>
                    </ul>
                    @if (session('status'))
                    <div class="card-body">
                        <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            @yield('usermenu')
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
