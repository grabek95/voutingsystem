@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/box-style.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container ">
        <div class="row">
            <aside class="col-12 text-center my-5 ">
                <h1> Głosowanie na najlepszego dziennikarza</h1>
                <p>Dziennikarstwo – działalność polegająca na zbieraniu i upublicznianiu za pomocą środków masowej komunikacji informacji o wydarzeniach, ludziach oraz problemach.
                <p> Oddaj głos na swojego kandydata!</p>
            </aside>
            <div class="col-md-8">
                 @include('_candidatesTableForm')
            </div>
            <div class="col-md-4">
                <form method="POST" action="{{route('vote.results')}}">
                    @csrf
                    <div class="table-box ">
                        <div class="card card-gray">
                            <div class="card-header">
                                <h3 class="card-title">Zagłosuj</h3>
                            </div>
                            <div class="margins">
                                <ul class="list-group">
                                    @foreach($candidates as $candidate)
                                        <li class="list-group-item"><input type="checkbox" name="candidates[]" value="{{$candidate->id}}" >{{$candidate->name}}</li>
                                    @endforeach
                                </ul>
                                <div class="row">
                                    <div class="item-center" >
                                        @error('morethan5')
                                        <p class="error text-danger">{{$errors->first('morethan5')}}</p>
                                        @enderror
                                        @error('duplication')
                                        <p class="error text-danger">{{$errors->first('duplication')}}</p>
                                        @enderror
                                        <button class="button is link" type="submit">Zagłosuj!</button>
                                        @error('candidates')
                                        <p class="error text-danger">{{$errors->first('candidates')}}</p>
                                        @enderror
                                        @error('transaction')
                                        <p class="error text-danger">{{$errors->first('transaction')}}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
