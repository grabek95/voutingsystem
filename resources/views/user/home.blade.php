@extends('layouts.user')

@section('usermenu')
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        Hello {{ Auth::user()->name }}. You are logged in!
                    </div>
                </div>
            </div>
@endsection
