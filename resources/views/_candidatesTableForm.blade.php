<div class="table-box ">
    <div class="card card-gray">
        <div class="card-header">
            <h3 class="card-title">Kandydaci</h3>
        </div>
        <table id="table_id" class="table">
            <thead class="thead-dark">
            <tr>
                <td>Wizerunek</td>
                <td>Imię Nazwisko</td>
                <td>Opis</td>
            </tr>
            </thead>
            <tbody>
            @foreach($candidates as $candidate)
                <tr>
                    <td>
                        @if ($candidate->image)
                            <img class="rounded" src="{{ asset('storage/'.$candidate->image) }}"  class="img-thumbnail" width="125" />
                        @else
                            <img class="rounded" src="{{ asset('storage/images/without_image.jpg') }}"  class="img-thumbnail" width="125" />
                        @endif

                    </td>
                    <td>{{$candidate->name}}</td>
                    <td>{{$candidate->description}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @error('delete')
        <p class="text-danger">{{$errors->first('delete')}}</p>
        @enderror

    </div>
</div>
