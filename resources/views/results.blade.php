
@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/box-style.css') }}" rel="stylesheet">
@endsection
@section('head')
    <script>
        window.onload = function() {

            var dataPoints=[];
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Ranking kandydatów"
                },
                axisY: {
                    title: "Ilość Głosów"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.## głosóy",
                    dataPoints: dataPoints
                }]
            });
            $.ajax({
                type: "GET",
                url: '/getCandidates',
                success: function(candidates){
                    $.each(candidates,function (i,value) {
                        dataPoints.push({
                            label: value.name,
                            y: value.votes
                        });
                    });
                    chart.render();
                }
            });
        }
    </script>
@endsection
@section('content')

    <div class="container" >
        <div id="bg-color">
            <h2 class="item-center">Wyniki</h2>
            <div class="chart">
                <div id="chartContainer" class="chart-results-style"></div>
            </div>
            @include('_candidatesTableForm')
    </div>

@endsection
@section('scripts')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@stop