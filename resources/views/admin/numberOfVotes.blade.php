@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@stop
@section('content')
    <div id="wrapper">
        <div id="page">
            <div class="card card-gray">
                <div class="card-header">
                    <h3 class="card-title">Edycja maksymalnej liczby glosów</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" action="{{route('nov.update',$numberOfVotes)}}">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nov">Max liczba głosów</label>
                            <input class="input @error('number_of_votes') is-danger @enderror" type="text" name="number_of_votes" id="number_of_votes" value="{{$numberOfVotes->number_of_votes}}">
                            @error('number_of_votes')
                            <p class="help is-danger">{{$errors->first('number_of_votes')}}</p>
                            @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                    @error('edit')
                    <p class="help is-danger">{{$errors->first('edit')}}</p>
                    @enderror
                    @error('update')
                    <p class="help is-danger">{{$errors->first('update')}}</p>
                    @enderror
                </form>
            </div>
        </div>
    </div>
@endsection
