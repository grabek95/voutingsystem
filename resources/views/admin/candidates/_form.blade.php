    @csrf
    <div class="card-body">
        <div class="form-group">
            <label fot="Name">Imię i nazwisko:</label>
            <input class="input @error('name') is-danger @enderror" type="text" name="name" id="name" value=
            @if (Route::current()->getName() == 'candidate.edit')
                    "{{$candidate->name}}"
            @else
                "{{old('name')}}"
            @endif
            >
            @error('name')
            <p class="help is-danger">{{$errors->first('name')}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label  fot="Description">Opis:</label>
            <textarea class="textarea @error('description') is-danger @enderror" type="text" name="description" id="description">@if (Route::current()->getName() == 'candidate.edit'){{$candidate->description}}@else{{old('description')}}@endif</textarea>
            @error('description')
            <p class="help is-danger">{{$errors->first('description')}}</p>
            @enderror
        </div>
        <div class="form-group">
            <label fot="image">Dodaj wizerunek kandydata</label>
            <div class="col-md-8">
                <input type="file" name="image"/>
                @error('image')
                <p class="help is-danger">{{$errors->first('image')}}</p>
                @enderror
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
