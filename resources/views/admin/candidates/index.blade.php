@extends('adminlte::page')
@section('css')
    <link href="{{ asset('css/box-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@stop

@section('content')
    <div class="card card-gray">
        <div class="card-header">
            <h3 class="card-title">Kandydaci</h3>
        </div>
        <div class="card-body">
             <table id="table_id" class="display">
                <thead>
                <tr>
                    <td>Nr</td>
                    <td>Imię Nazwisko</td>
                    <td>Liczba głosów</td>
                    <td>Zobacz głosy</td>
                    <td>Edytuj kandydata</td>
                    <td>Usuń kandydata</td>
                </tr>
                </thead>
                <tbody>
                @foreach($candidates as $candidate)
                    <tr>
                        <td>{{$candidate->id}}</td>
                        <td>{{$candidate->name}}</td>
                        <td>{{count($candidate->votes)}}</td>
                        <td>
                            <div class="button">
                                <form action="{{route('candidate.show',$candidate->id)}}"method="GET">
                                    <button type="submit" class="btn btn-block btn-primary btn-sm">Głosy</button>
                                </form>
                            </div>
                        </td>
                        <td>
                            <div class="button">
                                <form action="{{route('candidate.edit',$candidate)}}"method="GET">
                                    <button type="submit" class="btn btn-block btn-warning btn-sm">Edytuj</button>
                                </form>
                            </div>
                        </td>
                        <td>
                            <div class="button">
                                <button class="btn btn-block btn-danger btn-sm"  data-toggle="modal" data-target="#delete">Usuń</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal delete -->

    <div class="modal fade show" id="delete" >
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Usunięcie kandydata</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="{{route('candidate.delete',$candidate)}}" method="post">
                    @method('delete')
                    @csrf
                    <div class="modal-body">
                        <p class="text-center">
                            Czy napewno chce usunąć kandydata?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Anuluj</button>
                        <button type="submit" class="btn btn-dark">Usuń</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection
@section('js')
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
@stop

