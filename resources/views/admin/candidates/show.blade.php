@extends('adminlte::page')
@section('css')
    <link href="{{ asset('css/box-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
@stop

@section('content')

    <div class="card card-gray">
        <div class="card-header">
            <h3  class="card-title">{{$candidate->name}}</h3>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"> {{$candidate->description }}</li>
            <li class="list-group-item">Suma głosów: {{count($candidate->votes)}}</li>
        </ul>
        <div class="card-body" >
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <td>Nr</td>
                        <td>IP</td>
                        <td>Data</td>
                        <td>Usuń</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($candidate->votes as $vote)
                        <tr>
                            <td>{{$vote->id}}</td>
                            <td>{{$vote->ip_adress}}</td>
                            <td>{{$vote->created_at}}</td>
                            <td>
                                <div class="button">
                                    <form action="{{route('vote.delete',$vote)}}"method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-block btn-danger btn-sm">Usuń</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                @endforeach
                </tbody>
                @error('delete')
                <p class="help is-danger">{{$errors->first('delete')}}</p>
                @enderror
            </table>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
@stop
