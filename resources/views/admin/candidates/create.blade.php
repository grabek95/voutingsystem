@extends('adminlte::page')

@section('css')
    <link href="{{ asset('css/box-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@stop

@section('content')
    <div id="wrapper">
        <div id="page">
            <div class="card card-secondary">
                <div class="card-header">
                    <h3 class="card-title">Nowy kandydat</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="POST" enctype="multipart/form-data"  action={{route('candidate.store')}}>
                    @include('admin/candidates/_form')
                    @error('store')
                        <p class="help is-danger">{{$errors->first('create')}}</p>
                    @enderror
                </form>
            </div>
        </div>
    </div>
@endsection
