<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//['register'=> false]
Auth::routes();
//Admin
Route::group(['middleware' => [ 'role:admin']], function() {
    Route::group(['prefix'=>'admin'],function() {
        Route::resource('roles','RoleController');
        Route::resource('users','UserController');
        Route::get('/home', 'HomeController@index')->name('home')->middleware('auth.basic');
    //candidate
        Route::get('/wyniki', 'Admin\CandidateController@index')->name('candidate.results')->middleware('auth.basic');
        Route::get('/wyniki/{candidate}', 'Admin\CandidateController@show')->name('candidate.show')->middleware('auth.basic');
        Route::get('/dodaj', 'Admin\CandidateController@create')->name('candidate.create')->middleware('auth.basic');
        Route::post('/save', 'Admin\CandidateController@store')->name('candidate.store')->middleware('auth.basic');
        Route::get('/candidate/edit/{candidate}', 'Admin\CandidateController@edit')->name('candidate.edit')->middleware('auth.basic');
        Route::put('/candidate/{candidate}', 'Admin\CandidateController@update')->name('candidate.update')->middleware('auth.basic');
        Route::delete('/delete/candidate/{candidate}', 'Admin\CandidateController@destroy')->name('candidate.delete')->middleware('auth.basic');
    //nov
        Route::get('/nov/edit/', 'Admin\NumberOfVotesController@edit')->name('nov.edit')->middleware('auth.basic');
        Route::put('/nov/update/{numberOfVotes}', 'Admin\NumberOfVotesController@update')->name('nov.update')->middleware('auth.basic');
    //vote
        Route::delete('/delete/vote/{vote}', 'Admin\VoteController@destroy')->name('vote.delete')->middleware('auth.basic');
    });
});
Route::group(['middleware' => ['role:user']], function() {

    Route::get('/home','User\UserAccountController@index')->name('user.home')->middleware('auth.basic');;
});

//User

Route::get('/getCandidates','CandidateController@results')->name('vote.cr');
Route::get('/wyniki','CandidateController@index')->name('vote.results');

//vote
Route::get('/', 'VoteController@create')->name('vote.create');
Route::post('/wyniki','VoteController@store')->name('vote.store');



